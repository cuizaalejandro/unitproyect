import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Models.SoldProduct;

public class SoldProductTest {

	
	private   SoldProduct product ;
	@Before
	public void initProduct()
	{
		 product = new SoldProduct("is a good product ",10,12,"US" );
		 
		
		
		
	}
	
	@Test
	public void WhenGetQuantityASoldProductIs12() {
		
		int quantity = product.getQuantity();
		int expected = 12;
		assertEquals(expected, quantity);
	}


	@Test
	public void WhenGetCurrencyASoldProductIs() {
		
		String currency = product.getCurrency();
		String expected = "US";
		assertEquals(expected, currency);
	}
	
	
	@Test
	public void WhenSetQuantityASoldProductTo17() {
		
		product.setQuantity(17);
		int result = product.getQuantity();
		int expected = 17;
		assertEquals(expected, result);
	}
	
	@Test
	public void WhenSetCurrencyASoldProductToBs() {
		
		product.setCurrency("Bs");
		String result = product.getCurrency();
		String expected = "Bs";
		assertEquals(expected, result);
	}
	
	@Test
	public void WhenSetSoldDateASoldProductToMarch12() {
		
		product.setSoldDate("12/04/2020");
		String result = product.getSoldDate();
		String expected = "12/04/2020";
		assertEquals(expected, result);
	}
	
	@Test
	public void WhentoReturnToString ()
	{
		String result = product.toString();
		String expected = "SoldProduct{description=is a good product , unitPrice=10.0, quantity=12, currency=US}";
		assertEquals(expected, result);
		
	}
	
	@Test
	public void WhenSoldProductisNUll()
	{
		SoldProduct productNull = new SoldProduct();
		assertNull(productNull.getCurrency());
		assertNull(productNull.getDescription());
		assertNull(productNull.getSoldDate());
		
		
	}
	




}
