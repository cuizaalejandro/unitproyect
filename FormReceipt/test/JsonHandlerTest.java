import static org.junit.Assert.*;

import java.io.IOException;
import java.time.Year;
import java.util.LinkedHashMap;

import org.junit.Test;

import DataAccess.JsonHandler;
import Models.Buyer;
import Utilities.JsonObject;

public class JsonHandlerTest {

	@Test
	public void WhenCreteFileShouldBeTrue() throws IOException {
		
		JsonObject jsonObject = new JsonObject();
		jsonObject.addPairToJson("name", "alejandro");
		JsonHandler jsonHandler = new JsonHandler(jsonObject);
		boolean result = jsonHandler.createFile("prueba");
		assertTrue(result);
	}
	@Test
	public void WhenGetFileShoouldBeLinkedHashMap() throws Exception{
		
		JsonObject jsonObject = new JsonObject();
		jsonObject.addPairToJson("name", "alejandro");
		JsonHandler jsonHandler = new JsonHandler(jsonObject);
		boolean result = jsonHandler.createFile("pruebaSegunda");
		LinkedHashMap<String,String> linkedHashMap = jsonHandler.getFile("pruebaSegunda");
		String name =linkedHashMap.get("name");
		String nameExpect = "alejandro";
		assertEquals(nameExpect,name);
	}
	@Test
	public void WhenaddToMapShoouldBeTrue() throws Exception{
		
		JsonObject jsonObject = new JsonObject();
		jsonObject.addPairToJson("name", "alejandro");
		jsonObject.addPairToJson("apellido", "laimme");
		Buyer buyer = new Buyer("alfonso", "124555","alfonso@gmail.com", "adresss", "cash", "212121124321");
		JsonHandler<Buyer> jsonHandler = new JsonHandler<Buyer>(jsonObject);
		boolean result = jsonHandler.addToMap(buyer);
		assertTrue(result);
	}

}
