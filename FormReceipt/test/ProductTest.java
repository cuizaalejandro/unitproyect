import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Models.Product;

public class ProductTest {

	private Product product;
	@Before
	public void InitProduct()
	{
		product = new Product("is a nice product", 12);
	}
	
	
	@Test
	public void WhenGetdescriptionASoldProductIsGoodProduct() {
		
		String description = product.getDescription();
		String expected ="is a nice product";
		assertEquals(expected, description);
	}

	@Test
	public void WhenGetUnitPriceASoldProductIs10() {
		
		double unitPrice = product.getUnitPrice();
		double expected = 12;
		assertTrue(unitPrice == expected);
	}
	
	@Test
	public void WhenSetUnitPriceTo122()
	{
		product.setUnitPrice(122);
		double newUnitPrice = product.getUnitPrice();   
		double expected = 122;
		assertTrue(expected == newUnitPrice);
	}
	@Test
	public void WhenSetDescriptionTo122()
	{
		product.setDescription("good product");
		String description = product.getDescription();   
		String expected = "good product";
		assertEquals(expected,description);
	}
	
	
	@Test
	public void WhentoString()
	{
		String result = product.toString();
		String expected = "Product{unitPrice=12.0, description=is a nice product}";
		assertEquals(expected, result);
	}
	
	@Test
	public void WhenProductIsNull()
	{
		Product productNull = new Product();
		assertNull(productNull.getDescription());
		assertTrue(productNull.getUnitPrice() == 0.0);
	}	
	
	
}
