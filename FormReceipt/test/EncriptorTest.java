import static org.junit.Assert.*;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.junit.Assert;
import org.junit.Test;

import Utilities.Encryptor;

public class EncriptorTest {

	@Test
	public void wordEncryptShouldBeEncrypted() throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		String word = "Encrypt";
		String expected = "IVaMWrlBqAY+YTaU8KeNiQ==";
		String result = Encryptor.encryptValue(word);
		assertEquals(expected,result);
	}
	
	@Test
	public void wordEncryptedShouldBeDecrypted() throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		Encryptor encryptor = new Encryptor();
		String word = "IVaMWrlBqAY+YTaU8KeNiQ==";
		String expected = "Encrypt";
		assertEquals(encryptor.decryptValue(word),expected);
	}

	



}
