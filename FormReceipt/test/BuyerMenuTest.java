import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import Models.Buyer;

import java.io.*;
import java.util.LinkedHashMap;

import Presentation.BuyerMenu;

public class BuyerMenuTest {
	private final InputStream systemIn = System.in;
    private final PrintStream systemOut = System.out;

    private ByteArrayInputStream testIn;
    private ByteArrayOutputStream testOut;
    
    private BuyerMenu buyerMenu;
    @Before
    public void setUpOutput() {
        testOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOut));
        buyerMenu = new BuyerMenu();
    }
    private void provideInput(String data) {
        testIn = new ByteArrayInputStream(data.getBytes());
        System.setIn(testIn);
    }


    private String getOutput() {
        return testOut.toString();
    }
    @After
    public void restoreSystemInputOutput() {
        System.setIn(systemIn);
        System.setOut(systemOut);
    }
    

    @Test
	public void WhenRegisterMenu() {
		
		//BuyerMenu buyerMenu = new BuyerMenu();
		String fullName = "donal trump";
		String ci = "12345678";
		String extensionCi = "2";
		String email="alfonso@gmail.com";
		String adress = "adresssssss";
		String method = "1";
        provideInput(fullName+"\n"+ci+"\n" +" "+ extensionCi+"\n" +email+"\n"+adress +"\n"+ method+"\n");

      
		Buyer buyer = buyerMenu.registerMenu();
		String expectFullName = "donal trump";
		String expectCi = "12345678 - CHQ";
		String expectEmail = "alfonso@gmail.com";
		String expectAdress = "adresssssss";
		String expectMethod = "Cash";
		assertEquals(expectFullName, buyer.getFullName());
		assertEquals(expectCi, buyer.getCi());
		assertEquals(expectEmail, buyer.getEmail());
		assertEquals(expectAdress, buyer.getAddress());
		assertEquals(expectMethod,buyer.getPaymentMethod());
		
		
	}
	
	


}
