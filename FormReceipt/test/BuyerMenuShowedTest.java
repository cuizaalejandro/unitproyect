import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.LinkedHashMap;

import static org.mockito.Mockito.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import Presentation.BuyerMenu;

public class BuyerMenuShowedTest {
	private final InputStream systemIn = System.in;
    private final PrintStream systemOut = System.out;

    private ByteArrayInputStream testIn;
    private ByteArrayOutputStream testOut;
    
    private BuyerMenu buyerMenu;
    @Before
    public void setUpOutput() {
        testOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOut));
        buyerMenu = new BuyerMenu();
    }
    private void provideInput(String data) {
        testIn = new ByteArrayInputStream(data.getBytes());
        System.setIn(testIn);
    }


    private String getOutput() {
        return testOut.toString();
    }
    @After
    public void restoreSystemInputOutput() {
        System.setIn(systemIn);
        System.setOut(systemOut);
    }
    
	

	
	@Test
	public void WhenShowAndNotIsCreditNUmber()
	{
		LinkedHashMap<String, String> receiptData = new LinkedHashMap<String, String> ();
		
		receiptData.put("SoldDate", "12/19/20");
		receiptData.put("FullName", "alejandro");
		receiptData.put("Ci", "12345678");
		receiptData.put("Email", "alfonso@gmail.com");
		receiptData.put("Address", "adresssssss");
		receiptData.put("PaymentMethod", "Cash");
		receiptData.put("CreditCardNumber", " ");
		receiptData.put("Description", "description");
		receiptData.put("Quantity", "100");
		receiptData.put("UnitPrice", "12");
		receiptData.put("Currency", "Bs");
		buyerMenu.show(receiptData);
		String outputSoldate = getOutput();
		
	

		
		String expectedDates ="Buyer Details\n" + 
				"	Full Name                    alejandro\n" + 
				"	CI                           12345678\n" + 
				"	Email                        alfonso@gmail.com\n" + 
				"	Address                      adresssssss\n" + 
				"	Payment Method               Cash\n" + 
				"	Credit Card Number            \n";
		
		String expectedDescriptionProducts="Product Details\n" + 
				"	Product Description          description\n" + 
				"	Quantity                     100\n" + 
				"	Unit Price                   12\n" + 
				"	Currency                     Bs\n" + 
				"	                             \n" + 
				"	Total                        1200.0 Bs";
		String expected ="Create Date                  12/19/20"+"\n" + expectedDates+expectedDescriptionProducts+"\n";
		
		assertEquals(expected, outputSoldate );
		
		
	}

}
