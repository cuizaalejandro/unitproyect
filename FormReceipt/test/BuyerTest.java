import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Models.Buyer;

public class BuyerTest {

	private Buyer buyer;
	@Before
	public void InitABuyer()
	{
		buyer = new Buyer("alfonso", "124555","alfonso@gmail.com", "adresss", "cash", "212121124321");
		
	}
	@Test
	public void WhenGetPaymnetMethodShouldBeCash() {
		String paymentMethod = buyer.getPaymentMethod();
		String expected = "cash";
		assertEquals(expected,paymentMethod);
		
		
	}
	@Test
	public void WhenGetCreditcardShouldBe212121124321() {
		String creditCard = buyer.getCreditCardNumber();
		String expected = "212121124321";
		assertEquals(expected,creditCard);
		
		
	}
	
	
	@Test
	public void WhenSetPaymnetMethodShouldBeOnline() {
		buyer.setPaymentMethod("Online");
		String paymentMethod = buyer.getPaymentMethod();
		String expected = "Online";
		assertEquals(expected,paymentMethod);
		
		
	}
	@Test
	public void WhenSetCreditcardShouldBe12345() {
		buyer.setCreditCardNumber("12345");
		String creditCard = buyer.getCreditCardNumber();
		String expected = "12345";
		assertEquals(expected,creditCard);
		
		
	}
	
	@Test
	public void WhenToString() {
		String result = buyer.toString();
		String expected ="Buyer{fullName=alfonso, ci=124555, email=alfonso@gmail.com, address=adresss paymentMethod=cash, creditCardNumber=212121124321}";
		assertEquals(expected,result);
		
		
	}
	
	@Test
	public void WhenBuyerIsNUll() {
		Buyer buyerNull = new Buyer();
		assertNull(buyerNull.getPaymentMethod());
		assertNull(buyerNull.getCreditCardNumber());
		
		
		
	}

}
