import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Models.User;

public class UserTest {

	private User user;
	
	@Before
	public void initUser() 
	{
		user  = new User("alejandro","122121", "alejandro@gmail.com", "adresss");
	}
	
	@Test
	public void WhenGetNameALejandroShouldBeAlejandro() {
		String name = user.getFullName();
		String expected ="alejandro"; 
		assertEquals(name,expected);

		
		
	}
	@Test
	public void WhenGetNCi122121BeAlejandro() {
		String ci = user.getCi();
		String expected ="122121"; 
		assertEquals(ci,expected);

		
		
	}
	@Test
	public void WhenGetEmailShouldBealejandro() {
		
		String correo = user.getEmail();
		String expected ="alejandro@gmail.com"; 
		assertEquals(correo,expected);

		
		
	}
	@Test
	public void WhenGetAddresShouldBeAdress() {
		
		String address = user.getAddress();
		String expected ="adresss";
		assertEquals(address,expected);

		
		
	}
	
	@Test
	public void WhenSetFullNameAlejandroToSandro()
	{
	
	    user.setFullName("sandro");
	    String name = user.getFullName();
		String expected ="sandro";
		assertEquals(name,expected);
		
	}
	@Test
	public void WhenSetEmailAlejandroToAlvaro()
	{

	    user.setEmail("alvaro@gmail.com");
	    String email = user.getEmail();
		String expected ="alvaro@gmail.com";
		assertEquals(email,expected);
		
	}
	@Test
	public void WhenSetCi122121To1111111()
	{

	    user.setCi("1111111");
	    String ci = user.getCi();
		String expected ="1111111";
		assertEquals(ci,expected);
		
	}
	@Test
	public void WhenSetAdressToNewAdress()
	{
	
	    user.setAddress("new Adress");
	    String address = user.getAddress();
		String expected ="new Adress";
		assertEquals(address,expected);
		
	}
	
	@Test
	public void WhenReturnAStringJson()
	{

	    String result = user.toString();
	    String expected ="User{fullName=alejandro, ci=122121, email=alejandro@gmail.com, address=adresss}";
		assertEquals(result,expected);
		
	}
	@Test
	public void WhenUserIsNUll() {
		User user  = new User();
	    assertNull(user.getFullName());
	    assertNull(user.getCi());
	    assertNull(user.getEmail());
	    assertNull(user.getAddress());
		
	}
	
	
	
	

}
