import static org.junit.Assert.*;

import java.io.IOException;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import Utilities.JsonObject;

public class JsonObjectTest {


	@Test
	public void saveJsonFileShouldReturnTrue() throws IOException {
		JsonObject json = new JsonObject();
		String word = "Encrypt";
		boolean expected = true;
		assertEquals(json.saveJsonFile(word),expected);
	}
	
	@Test
	public void whenAddPairToJson() throws Exception {
		JsonObject json = new JsonObject();
		String word = "Encrypt";
		json.addPairToJson("name", "alejandro");
		json.saveJsonFile(word);
		JSONObject jsonResult = json.getJsonFile(word);
		JSONObject jsonexpect = new JSONObject();
		jsonexpect.put("name", "alejandro");
		assertTrue(jsonResult.equals(jsonexpect));
		
	}
	@Test
	public void whenGetSizeJson() throws Exception {
		JsonObject json = new JsonObject();
		String word = "Encrypt";
		json.addPairToJson("name", "alejandro");
		json.saveJsonFile(word);
		int result = json.size();
		int expected=1;
		assertEquals(expected,result);
		
	}


}
