import static org.junit.Assert.*;

import Utilities.Validator;

import org.junit.Test;

public class ValidatorTest {

	@Test
	public void shouldReturnERRORifCreditCardisNotCorrect() {
		Validator validator = new Validator();
		String word = "1111-2222-3333-4444-llblblb;";
		String expected = "The credit card is incorrect e.g. xxxx-xxxx-xxxx-xxxx";
		assertEquals(validator.verifyCreditCard(word),expected);
	}
	
	@Test
	public void shouldReturnValueOfCreditCardWhenCreditCardisCorrect() {
		Validator validator = new Validator();
		String word = "1234-5678-9012-3456";
		String expected = "";
		assertEquals(validator.verifyCreditCard(word),expected);
	}
	
	@Test
	public void shouldReturnMailIfMailIsCorrect() {
		Validator validator = new Validator();
		String word = "mail@mail.com";
		String expected = "";
		assertEquals(validator.verifyEmail(word),expected);
	}
	
	@Test
	public void shouldReturnMailIfMailIsNotCorrect() {
		Validator validator = new Validator();
		String word = "mail.com";
		String expected = "The email is incorrect, e.g. example@gmail.com";
		assertEquals(validator.verifyEmail(word),expected);
	}
	
	@Test
	public void shouldReturnSpaceIfCIisCorrect() {
		Validator validator = new Validator();
		String word = "123456";
		String expected = "";
		assertEquals(validator.validateCiNumber(word),expected);
	}
	
	@Test
	public void shouldReturnERRORIfCIisNotCorrect() {
		Validator validator = new Validator();
		String word = "asfasdff";
		String expected = "The field must have only integer numbers";
		assertEquals(validator.validateCiNumber(word),expected);
	}
	
	@Test
	public void shouldReturnERRORIfCIisEmpty() {
		Validator validator = new Validator();
		String word = "";
		String expected = "The field should be between 6 and 10 characters";
		assertEquals(validator.validateCiNumber(word),expected);
	}
	
	@Test
	public void shouldReturnErrorIfFullNameisShorterThan10() {
		Validator validator = new Validator();
		String word = "My Name";
		String expected = "The field should be between 10 and 100 characters";
		assertEquals(validator.validateFullName(word),expected);
	}
	
	@Test
	public void shouldReturnSpaceIfFullNameisBiggerThan100() {
		Validator validator = new Validator();
		char letter = 'a';
		String word = "";
		for(int i =0; i < 101; i++) {
			word += letter;
		}
		String expected = "The field should be between 10 and 100 characters";
		assertEquals(validator.validateFullName(word),expected);
	}
	
	@Test
	public void shouldReturnSpaceIfFullNameisCorrect() {
		Validator validator = new Validator();
		String word = "My Full Name";
		String expected = "";
		assertEquals(validator.validateFullName(word),expected);
	}
	
	@Test
	public void shouldReturnTrueifIsOnlyLetters(){
		Validator validator = new Validator();
		String value= "12345678901" ;
		String expected = "The field must have only letters";
		assertEquals(validator.validateFullName(value),expected);
	}

	@Test
	public void shouldReturnSpaceifDecimalIsCorrect(){
		Validator validator = new Validator();
		String value= "12345678901" ;
		String expected = "";
		String validation = "validateUnitPrice";
		assertEquals(validator.applyValidation(value, validation),expected);
	}
	
	@Test
	public void shouldReturnErrorifLenghtGreaterThan100(){
		Validator validator = new Validator();
		char letter = 'a';
		String value= "";
		for(int i =0; i < 101; i++) {
			value += letter;
		}
		String expected = "The field should be between 10 and 100 characters";
		String validation = "validateDescription";
		assertEquals(validator.applyValidation(value, validation),expected);
	}
	
	@Test
	public void shouldReturnErrorifLenghtSmoller10(){
		Validator validator = new Validator();
		char letter = 'a';
		String value= "";
		for(int i =0; i < 9; i++) {
			value += letter;
		}
		String expected = "The field should be between 10 and 100 characters";
		String validation = "validateDescription";
		assertEquals(validator.applyValidation(value, validation),expected);
	}
	
	@Test
	public void shouldReturnSpaceifLenghtbetween10and100(){
		Validator validator = new Validator();
		char letter = 'a';
		String value= "";
		for(int i =0; i < 51; i++) {
			value += letter;
		}
		String expected = "";
		String validation = "validateDescription";
		assertEquals(validator.applyValidation(value, validation),expected);
	}
	
	@Test
	public void applyValidationFullName(){
		Validator validator = new Validator();
		String value= "abcdefg hijklmn";
		String expected = "";
		String validation = "validateFullName";
		assertEquals(validator.applyValidation(value, validation),expected);
	}
	
	@Test
	public void applyValidationvalidateCiNumber(){
		Validator validator = new Validator();
		String value= "123456789";
		String expected = "";
		String validation = "validateCiNumber";
		assertEquals(validator.applyValidation(value, validation),expected);
	}
	
	@Test
	public void applyValidationverifyEmail(){
		Validator validator = new Validator();
		String value= "mail@mail.com";
		String expected = "";
		String validation = "validateEmail";
		assertEquals(validator.applyValidation(value, validation),expected);
	}
	
	@Test
	public void applyValidationvalidateAddress(){
		Validator validator = new Validator();
		String value= "abcdef ghijkl";
		String expected = "";
		String validation = "validateAddress";
		assertEquals(validator.applyValidation(value, validation),expected);
	}
	
	@Test
	public void applyValidationverifyCreditCard(){
		Validator validator = new Validator();
		String value= "1234 5678 9012 3456";
		String expected = "The credit card is incorrect e.g. xxxx-xxxx-xxxx-xxxx";
		String validation = "verifyCreditCard";
		assertEquals(validator.applyValidation(value, validation),expected);
	}
	
	@Test
	public void applyValidationverifyIsOnlyNumbers(){
		Validator validator = new Validator();
		String value= "1234567890";
		String expected = "";
		String validation = "validateQuantity";
		assertEquals(validator.applyValidation(value, validation),expected);
	}


}
