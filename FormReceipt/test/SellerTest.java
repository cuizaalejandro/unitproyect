import static org.junit.Assert.*;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.LinkedHashMap;

import Models.Buyer;
import Models.SoldProduct;
import Models.Interfaces.FileHandler;
import Models.Interfaces.ObjectHandler;
import Utilities.JsonObject;

import org.junit.Assert;
import org.junit.Test;

import BusinessLogic.Seller;
import DataAccess.JsonHandler;

public class SellerTest {

	@Test
	public void WhenSaveReceiptShouldBeTrue() throws GeneralSecurityException, Exception {
		
		JsonObject jsonObject = new JsonObject();
		jsonObject.addPairToJson("name", "alejandro");
		JsonHandler jsonHandler = new JsonHandler(jsonObject);
		Seller seller = new Seller(jsonHandler);
		Buyer buyer = new Buyer("alfonso", "124555","alfonso@gmail.com", "adresss", "cash", "212121124321");
		SoldProduct product = new SoldProduct("is a good product ",10,12,"US" );
		boolean result = seller.saveReceipt(buyer, product);
		assertTrue(result);
		
	}
	@Test
	public void WhenGetReceipttShouldBeLinkedHashMap() throws GeneralSecurityException, Exception {
		
		JsonObject jsonObject = new JsonObject();
		jsonObject.addPairToJson("name", "alejandro");
		jsonObject.addPairToJson("CreditCardNumber", "IVaMWrlBqAY+YTaU8KeNiQ==");
	
		JsonHandler jsonHandler = new JsonHandler(jsonObject);
		jsonHandler.createFile("pruebaSegunda");
		Seller seller = new Seller(jsonHandler);
		Buyer buyer = new Buyer("alfonso", "124555","alfonso@gmail.com", "adresss", "cash", "212121124321");
		SoldProduct product = new SoldProduct("is a good product ",10,12,"US" );
		LinkedHashMap<String,String> result = seller.getReceipt("pruebaSegunda");
		String creditCard =result.get("CreditCardNumber");
		String expected = "Encrypt";
		assertEquals(expected,creditCard);
		
		
	}
}
